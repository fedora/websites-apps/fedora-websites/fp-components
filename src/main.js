import components from './components';
import './index.styles.css';

export default {
  install: (app, options) => {
    Object.keys(components).forEach(key => {
      app.component(key, components[key]);
    });
  },
};
