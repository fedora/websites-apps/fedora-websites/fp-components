# Button

This is the documentation page for the awesome `<FpButton />` component.

## Usage

```vue{3}
<template>
  <FpButton>My Button</FpButton>
</template>
```

## Example

<script setup>
  import FpButton from './FpButton.vue';
</script>

<FpButton>My Button</FpButton>
